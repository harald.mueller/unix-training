#!/bin/bash
logfile="/mnt/c/tmp-unix/i-was-here.log"
syst=$(uname -n)
zeit=`date +"%Y-%m-%d %R"`
heute=`date +"%Y-%m-%d"`
osname=`cat /etc/os-release | grep PRETTY_NAME | cut -d "=" -f 2 | tr \" \ `
nichtheute=$(tail -n 1 $logfile | cut -d " " -f 1)
out="$zeit $syst $osname";
if [ "$heute" != "$nichtheute" ];
    then out="$out ---- neuer Tag ---- $heute";
fi
echo $out >> $logfile
echo -en "\007"
mpg123 /mnt/d/repo21/unix-training/gong/gong.mp3
