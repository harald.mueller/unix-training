#!/bin/bash
idx=20
while [ $idx -gt 9 ]; do
# while [ $idx > 9 ]; do      ## dies funktioniert nicht
        idx=$(($idx-1))       ## Variante 1  geht
    # ((idx=idx-1))           ## Variante 2  geht
    # ((idx--)) oder (--idx)) ## Variante 3  geht
    # let "idx=idx-1"         ## Variante 4  geht
    # idx='$idx-1'            ## Variante 5  geht nicht
    # idx=$(idx-1)            ## Variante 6  geht nicht
    # $idx=$($idx-1)          ## Variante 7  geht nicht
    # $idx=$idx-1             ## Variante 8  geht nicht
    # $idx - 1                ## Variante 9  geht nicht
    # $idx -=1                ## Variante 10 geht nicht
    # $idx=$idx-1             ## Variante 11 geht nicht
    # idx=$idx-1              ## Variante 12 geht nicht
    # idx=($idx-1)            ## Variante 13 geht nicht
   echo " Index ist $idx"
done
echo "--- das war 'LB1-Aufg-2c.sh' mit dem -gt 9 Test ---"
