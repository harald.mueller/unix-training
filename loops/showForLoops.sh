#!/bin/bash
counter=1
# for i in {/tmp/} ; do       # geht nicht
# for i in ${ls /tmp} ; do    # geht nicht
# for i in $(ls /var/) ; do   # funktioniert
# for i in `ls /var/` ; do    # funktioniert
  for i in /var/* ; do          # funktioniert
    echo "$counter -- $i"
    counter=$((counter+1))
  done
  echo " ---- counter = $counter "

counter=$((counter+1))   ; echo "counter ist nun $counter"
((counter=counter+1))    ; echo "counter ist nun $counter"
((counter+=1))           ; echo "counter ist nun $counter"
((counter++))            ; echo "counter ist nun $counter"
let "counterc=counter+1" ; echo "counter ist nun $counter"
let "counter+=1"         ; echo "counter ist nun $counter"
let "counter++"          ; echo "counter ist nun $counter"
$counter=$counter+1      ; echo "counter ist nun $counter" # geht leider nicht
