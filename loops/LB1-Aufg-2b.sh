#!/bin/bash

for i in $(ls /tmp)   ; do      ##  Variante 1 geht
 # for i in /tmp/*    ; do      ##  Variante 2 geht
 # for i in /tmp/     ; do      ##  Variante 3 geht nicht
 # for i in /tmp      ; do      ##  Variante 4 geht nicht
 # for i in ls /tmp/  ; do      ##  Variante 5 geht nicht
 # for i in {/tmp/}   ; do      ##  Variante 6 geht nicht
 # for i in {ls /tmp/}; do      ##  Variante 7 geht nicht
 # for i in $/tmp/    ; do      ##  Variante 8 geht nicht
 # for i in $(grep -r /tmp); do ## macht etwas anderes, listet alle Fileinhalte auf
   echo $i
done
echo "--- das war 'LB1-Aufg-2b.sh' mit $ (ls /tmp) ---"
