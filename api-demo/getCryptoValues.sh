#!/bin/bash

date=$(date '+%Y-%m-%d_%H:%M')

kurs1=$(curl https://api.gemini.com/v2/ticker/btcusd | cut -d ":" -f9 | cut -d "\"" -f2)
logline+=$(printf "|%-16s | %10s |" $date $kurs1)
logline+=$'\n'

kurs2=62222.22
logline+=$(printf "|%-16s | %10s |" $date $kurs2)
logline+=$'\n'

kurs3=63333.33
logline+=$(printf "|%-16s | %10s |" $date $kurs3)

echo -e "${logline}"

if [[ $1 = "-f" ]]
then    # es wurde -f angegeben
        echo -e "${logline}" >> btc.log
fi
