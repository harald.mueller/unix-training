#!/bin/bash

logfile=""
if [ -z $1 ]; then
  logfile=/home/hmueller/unix-training/api-demo/rates.log
else
  logfile=$1
fi


chfUSD=$(curl -s https://open.er-api.com/v6/latest/CHF | sed 's/,/\n/g' | grep USD | cut -d : -f 2)
chfEUR=$(curl -s https://open.er-api.com/v6/latest/CHF | sed 's/,/\n/g' | grep EUR | cut -d : -f 2)
chfHUF=$(curl -s https://open.er-api.com/v6/latest/CHF | sed 's/,/\n/g' | grep HUF | cut -d : -f 2)

btcUSD=$(curl -s https://api.coinstats.app/public/v1/markets?coinId=bitcoin  | sed 's/{/\n/g' | grep Coinbase | grep -w BTC/USD | cut -d : -f 2 | cut -d , -f 1)
ethUSD=$(curl -s https://api.coinstats.app/public/v1/markets?coinId=ethereum | sed 's/{/\n/g' | grep Coinbase | grep -w ETH/USD | cut -d : -f 2 | cut -d , -f 1)

tsp=$(date +"%Y-%m-%d_%T")
echo -e  "\n ______________________________________________ $tsp __________________    >> $logfile
printf "| 1 CHF %10.5f USD | 1 USD %11.5f CHF |\n" $chfUSD $(echo "1/$chfUSD" | bc -l) >> $logfile
printf "| 1 CHF %10.5f EUR | 1 EUR %11.5f CHF |\n" $chfEUR $(echo "1/$chfEUR" | bc -l) >> $logfile
printf "| 1 CHF %10.3f HUF | 1 HUF %11.5f CHF |\n" $chfHUF $(echo "1/$chfHUF" | bc -l) >> $logfile
printf "| 1 BTC %10.1f USD | 1 USD %11.9f BTC |\n" $btcUSD $(echo "1/$btcUSD" | bc -l) >> $logfile
printf "| 1 ETH %10.2f USD | 1 USD %11.9f ETH |\n" $ethUSD $(echo "1/$ethUSD" | bc -l) >> $logfile
# printf " ______________________________________________\n"                         >> $logfile


printf "%12.2f %20s\n" $btcUSD $tsp >> /home/hmueller/unix-training/api-demo/btc.log

$koka=curl -s "https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=KO&apikey=SSZF5PJMC9V5QNH2&datatype=csv"
## $c.RawContent
## Api-Key  alphavantage
## SSZF5PJMC9V5QNH2
echo $koka

