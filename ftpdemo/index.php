<?php
$backcolRed = "#E5E4E2";
$backcolYellow = "#E5E4E2";
$backcolGreen = "#E5E4E2";
$stat = "";
switch (rand(0,2)) {
	case 2:
		$backcolRed = "red";
		$stat = "STOP";
		break;
	case 1:
		$backcolYellow = "#FF8C00";
		$stat = "WARN";
		break;
	default:
		$backcolGreen = "#16F529";
		$stat = "GO";
}
?><html>
 <head>
	<meta charset="utf-8">
	<style>
	* { font-family: arial;
		margin: 15px; }
    .circle {
      width: 35px;
      height: 35px;
      -webkit-border-radius: 35px;
      -moz-border-radius: 35px;
      border-radius: 35px;
	  border: solid 2px #555;
    }	
	</style>
 </head>
 <body>
  <center>
	<br><?= $_SERVER['SERVER_NAME'] ?><?= $_SERVER['PHP_SELF']; ?>
	<br>
	<br><?= date("d.m.Y H:i:s"); ?>
	<div style="border: solid 2px #555; width: 70px;">
		<div class="circle" style="background-color: <?= $backcolRed ?>;"></div>
		<div class="circle" style="background-color: <?= $backcolYellow ?>;"></div>
		<div class="circle" style="background-color: <?= $backcolGreen ?>;"></div>
		<div style="text-align: center;"><?= $stat ?></div>
	</div>
  </center>
 </body>
</html>