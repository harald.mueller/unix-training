#!/bin/bash

echo "-- Beginn Transfer --"

ftpHOST="ftp.haraldmueller.ch"
ftpUSER="schueler"
ftpPASS="studentenpasswort"
ftpDIR="haraldmueller"


ftp -inv $ftpHOST << MARKER

quote USER $ftpUSER
quote PASS $ftpPASS

cd $ftpDIR

mput *.log
mget index.*

close
bye

MARKER

echo "-- Ende Transfer --"
