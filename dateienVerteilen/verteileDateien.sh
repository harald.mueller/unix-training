#!/bin/bash

rm -drf _gen
mkdir _gen

for klasse in $(ls _schulklassen)
do
   klassenname=$(echo $klasse | cut -d "." -f 1)
   mkdir "_gen/$klassenname"
   for schueler in $(cat _schulklassen/$klasse)
   do
	mkdir "_gen/$klassenname/$schueler"
	cp _templates/* "_gen/$klassenname/$schueler"
   done
done
