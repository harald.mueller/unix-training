#!/bin/bash
inpFile="./data_formatting/url-list.txt"
resFile="./data_formatting/url-results.txt"
printf "bad output" > $resFile
while read line
do
    printf "$line" >> $resFile
    printf "\t\t\t\t" >> $resFile                                  # mit einfachen Tabulatoren
    curl -o /dev/null --silent --head --write-out '%{http_code} %{time_total}' "$line" >> $resFile
    printf "\n" >> $resFile
done <"$inpFile"


printf "\n\nbetter" >> $resFile
fmt="%-25s%-12s%-12s\n"                                            # Format-Beschreibung
printf "$fmt" DOMAIN_NAME HTTP_CODE RESPONSE_TIME >> $resFile      # Titelzeile
while read line
do
    read code time < <(curl -o /dev/null --silent --head --write-out '%{http_code} %{time_total}' "$line")
    printf "$fmt" "$line" "$code" "$time" >> $resFile
done <"$inpFile"
