#!/bin/bash

function round05(){

	r=$(echo "$1 * 20" | bc -l)
	s=$(echo "($r + 0.5)/1" | bc -l)
	printf -v zwischenresultat %.f $(echo "$s")
	printf -v res %.2f $(echo "$zwischenresultat/20" | bc -l)
	echo "$res"
}

round05 $1


#--- tests ---
#round05 192.09
#round05 192.08
#round05 192.07
#round05 192.06
#round05 192.05
#round05 192.04
#round05 192.03
#round05 192.02
#round05 192.01

