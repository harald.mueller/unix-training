#!/bin/bash

grandtotal=0
printf "\n\n"
printf "|%3s|%-25s|%8s|%9s|%15s|\n" "Pos" "Artikelbeschreibung" "Anzahl" "Preis" "Total"
printf "|---+-------------------------+--------+---------+---------------|\n"

	nr=1
	txt="Crazy-Socks, yellow"
	anz=3.0
	preis=14.99
	summe=$(echo "$anz * $preis" | bc)
	total=$( ./round05.sh $summe )
	grandtotal=$(echo "$grandtotal + $total" | bc)
	printf "%3s  %-25s %8s %9s %15s \n" $nr "$txt" $anz $preis $total

	nr=2
	txt="nylon rope 8mm, per m"
	anz=4.5
	preis=2.13
	summe=$(echo "$anz * $preis" | bc)
	total=$( ./round05.sh $summe )
	grandtotal=$(echo "$grandtotal + $total" | bc)
	printf "%3s  %-25s %8s %9s %15s \n" $nr "$txt" $anz $preis $total

	nr=3
	txt="coffee bag black magic"
	anz=2.0
	preis=4.82
	summe=$(echo "$anz * $preis" | bc)
	total=$( ./round05.sh $summe )
	grandtotal=$(echo "$grandtotal + $total" | bc)
	printf "%3s  %-25s %8s %9s %15s \n" $nr "$txt" $anz $preis $total

printf "%65s\n" "---------------"
printf "%65s\n" $grandtotal
printf "                                                  ===============\n"
printf "\n"


